<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('index');

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('cek_email_aktif', 'Auth\LoginController@cek_email_aktif')->name('cek_email_aktif');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/home', 'Member\MemberController@StructureMember')->name('home');
// Route::get('/', 'Member\MemberController@StructureMember')->middleware('auth')->name('member.structure');

Route::post('register/store', 'Auth\RegisterController@store')->name('register.store');

// Route::get('payment/confirmation/{email}', 'Payment\PaymentConfirmationController@index')->name('payment.confirmation.index');
Route::get('payment/confirmation', 'Payment\PaymentConfirmationController@index')->name('payment.confirmation.index');
Route::post('payment/confirmation/create', 'Api\Cashback\CashbackController@storePaymentConfirmation')->name('payment.confirmation.create');
// Route::post('payment/confirmation/create', 'Payment\PaymentConfirmationController@create')->name('payment.confirmation.create');
Route::get('payment/credit-card', 'Payment\PaymentCreditCardController@index')->name('payment.credit_card');

//email
Route::get('get_data_email/{base64}', 'Member\Services\MailService@get_data_email')->name('get_data_email');
// Route::get('register_after_send_email/{base64}', 'Member\Services\MailService@register_after_send_email')->name('register_after_send_email');

//checkout
Route::post('checkout/{customer_code}/group/{user_groups}', 'Member\Services\MailService@checkout')->name('checkout');


Route::prefix('member')->name('member.')->group(function() {
    Route::get('structure', 'Member\MemberController@StructureMember')->middleware('auth')->name('structure');
    Route::get('structure_one/{id}', 'Member\MemberController@StructureMemberOne')->middleware('auth')->name('structure_one');
    Route::get('create', 'Member\MemberController@create')->name('create');
    Route::post('storeMember', 'Member\MemberController@storeMember')->name('storeMember');
    Route::get('create/{customer_code}/group/{user_groups}', 'Member\MemberController@create_member')->name('create_member');
    Route::post('storeCustomer', 'Member\MemberController@storeCustomer')->name('storeCustomer');
    Route::get('cek_ktp', 'Member\MemberController@cek_ktp')->name('cek_ktp');
    Route::get('cek_email', 'Member\MemberController@cek_email')->name('cek_email');
    // Route::get('cek_email_aktif', 'Member\MemberController@cek_email_aktif')->name('cek_email_aktif');
    Route::get('update_status_email/{email}', 'Member\Services\MailService@update_status_email')->name('update_status_email');
    // Route::get('ref_code', 'Member\MemberController@ref_code')->name('ref_code');

});

// Route::get('data_email/{email}', 'Member\Services\MailService@update_status_email')->name('update_status_email');

// Route::get('/verify/{token}', 'verifyController@verify')->name('verify');

// Route::post('send',[
//     'as' => 'send.email',
//     'uses' => 'Member\Services\Member_services@sendEmail'
// ]);



