<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1/cashback')->group(function () {
    Route::get('packets', 'Api\Cashback\CashbackController@getPackets')->name('getPackets');
    Route::get('provinces', 'Api\Cashback\CashbackController@getProvinces')->name('getProvinces');
    Route::get('cities', 'Api\Cashback\CashbackController@getCities')->name('getCities');
    Route::get('districts', 'Api\Cashback\CashbackController@getDistricts')->name('getDistricts');
    Route::get('villages', 'Api\Cashback\CashbackController@getVillages')->name('getVillages');
    Route::post('customers/store', 'Api\Cashback\CashbackController@storeCustomer')->name('storeCustomer');
    Route::post('payments/confirmation/store', 'Api\Cashback\CashbackController@storePaymentConfirmation')->name('storePaymentConfirmation');
    Route::get('check/ktp', 'Api\Cashback\CashbackController@checkKtp')->name('checkKtp');
    Route::get('check/email', 'Api\Cashback\CashbackController@checkEmail')->name('checkEmail');
    Route::get('check/referral-code', 'Api\Cashback\CashbackController@checkReferralCode')->name('checkReferralCode');
    Route::post('members/store', 'Api\Cashback\CashbackController@storeMember')->name('storeMember');

});
