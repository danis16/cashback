@extends('layouts.auth')

@section('title')
<title>Cashback - Register</title>
@endsection

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
.error {
    color: #dc3545;
}

.form-control:focus {
    border-color: #007bff;
    box-shadow: inset 0 1px 1px rgba(40, 167, 69, 0.25), 0 0 8px rgba(40, 167, 69, 0.6);
}

.custom-radio .custom-control-input:checked~.custom-control-label::before,
.custom-radio .custom-control-input:checked~.custom-control-label::after {
    background-color: #007bff;
    border-radius: 50%;
}

.custom-radio .custom-control-input:focus~.custom-control-label::before {
    box-shadow: 0 0 0 1px #fff, 0 0 0 0.2rem rgba(40, 167, 69, 0.25);
}

.custom-checkbox .custom-control-input:checked~.custom-control-label::before,
.custom-checkbox .custom-control-input:checked~.custom-control-label::after {
    background-color: #007bff;
    border-radius: 30%;
}

.custom-checkbox .custom-control-input:focus~.custom-control-label::before {
    box-shadow: 0 0 0 1px #fff, 0 0 0 0.2rem rgba(40, 167, 69, 0.25);
}
</style>
@endpush

@section('content')
<section class="fdb-block">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 col-lg-8 col-xl-6">
                <div class="row">
                    <div class="col text-center">
                    <h1>Create Cashback Account</h1>
                        <p class="text-h3">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia. </p>
                    </div>
                </div>
                @if(session()->has('alert'))
                <div class="row">
                    <div class="alert alert-danger" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! session('alert') !!}
                    </div>
                </div>
                @endif
                <div class="row justify-content-center">
                    <form class="register" id="form-register" method="post" action="{{ url('checkout') }}/{{$data->customer_code}}/group/{{$data->user_groups}}" aria-label="{{ __('Register') }}">
                        @csrf
                        <hr class="mb-4">
                        <div class="mb-3">
                            <label for="packets">Member Type<span class="text-danger">*</span></label>
                            <div class="d-block member-type"></div>
                            <div class="price"></div>
                            <div class="packets-error"></div>
                        </div>
                        <div class="mb-3">
                            <label for="referral_code">Referral Code</label>
                            <input type="text" class="form-control" id="referral_code" name="referral_code" value="{{$data->referral_code}}" readonly>
                            <div class="label-code"></div>
                        </div>
                        <hr class="mb-4">
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="first_name">First Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="first_name" name="first_name"  value="{{$data->first_name}}" readonly>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="last_name">Last Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="last_name" name="last_name" value="{{$data->last_name}}" readonly>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="ktp">No KTP<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="ktp" name="ktp" maxlength="16" value="{{$data->ktp}}" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="email">Email<span class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" name="email" value="{{$data->email}}" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="gender">Gender<span class="text-danger">*</span></label>
                            <div class="d-block">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input id="male" name="gender" type="radio" class="custom-control-input" data-error=".gender-error" value="M" {{ $data->gender == "M" ? 'checked' : '' }} >
                                    <label class="custom-control-label" for="male">Male</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input id="female" name="gender" type="radio" class="custom-control-input" data-error=".gender-error" value="F" {{$data->gender == "F" ? 'checked' : '' }} >
                                    <label class="custom-control-label" for="female">Female</label>
                                </div>
                            </div>
                            <div class="gender-error"></div>
                        </div>
                        <div class="mb-3">
                            <label for="birth_date">Date of Birth<span class="text-danger">*</span></label>
                            <input type="date" class="form-control" id="birth_date" name="birth_date" placeholder="DD-MM-YYYY" value="{{$data->birth_date}}" readonly>
                        </div>
                        <div class="mb-3">
                            <label for="phone">Mobile Phone No<span class="text-danger">*</span></label><br>
                            <input type="tel" class="form-control" id="phone" name="phone" maxlength="13" value="{{$data->phone}}" readonly>
                            {{-- <input type="hidden" value="{{$data->customer_code}}" name="customer_code" > --}}
                        </div>
                        <div class="mb-3">
                            <label for="address">Address<span class="text-danger">*</span></label>
                            <textarea class="form-control" id="address" name="address" rows="3" readonly>{{$data->address}}</textarea>
                        </div>
                        <hr class="mb-4">
                            {{-- <label for="">Change Password</label> --}}
                        <div class="mb-3">
                            <label for="password">Password<span class="text-danger">*</span></label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <div class="mb-3">
                            <label for="confirm_password">Confirm Password<span class="text-danger">*</span></label>
                            <input type="password" class="form-control" id="confirm_password" name="confirm_password">
                        </div>
                        <hr class="mb-4">
                        <div class="mb-3">
                            <label for="payment_method">Payment Method<span class="text-danger">*</span></label>
                            <div class="d-block">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input id="atm" name="payment_method" type="radio" class="custom-control-input" value="1" data-error=".payment_method-error">
                                    <label class="custom-control-label" for="atm">ATM Transfer</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input id="credit_card" name="payment_method" type="radio" class="custom-control-input" value="2" data-error=".payment_method-error">
                                    <label class="custom-control-label" for="credit_card">Credit Card</label>
                                </div>
                            </div>
                            <div class="payment_method-error"></div>
                        </div>
                        <hr class="mb-4">
                        <div class="mb-3">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="terms" name="terms" data-error=".terms-error">
                                <label class="custom-control-label" for="terms">I agree to Terms & Condition and Privacy Policy.</label>
                            </div>
                            <div class="terms-error"></div>
                        </div>
                        <hr class="mb-4">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/additional-methods.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
function getProvinces() {
    $.ajax({
        type: 'GET',
        url: '{{ route('getProvinces') }}',
        dataType: 'json',
        success: function(response) {
            if (response.message = 'success') {
                $.each(response.data, function(i, data) {
                    $('#province').append('<option value="' + data.name + '" data-province-id="' + data.id + '">' + data.name + '</option>');
                });
            }
        },
        error: function(err) {
            console.log(err);
        }
    });
}

$(document).ready(function() {
    $("#form-register").validate({
        ignore: [],
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            ktp: {
                required: true,
                digits: true,
                minlength: 16            },
            email: {
                required: true,
                email: true            },
            gender: {
                required: true
            },
            birth_date: {
                required: true
            },
            phone: {
                required: true,
                digits: true
            },
            address: {
                required: true
            },
            packets: {
                required: true
            },
            referral_code: {
                remoteCheckReferralCode: true
            },
            password: {
                required: true
            },
            confirm_password: {
                required: true,
                equalTo: '#password'
            },
            terms: {
                required: true
            },
            payment_method: {
                required: true
            },
        },
        messages: {
            referral_code: {
                remote: "Referral code does not exists."
            }
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });

    $.validator.addMethod("remoteCheckReferralCode", function(value, element) {
        res = $.ajax({
            url: "{{ route('checkReferralCode') }}",
            method: "GET",
            data: {
                referral_code: function() {
                    return $("#referral_code").val();
                }
            },
            dataType: 'json',
            success: function(data) {
                if (data == true) {
                    $('.label-code').html('<div style="color: green;">You can use this referral code.</div>')
                    $('#referral_code').addClass('valid').removeClass('error')
                } else if (data == null) {
                    $('.label-code').html('')
                    $('#referral_code').addClass('valid').removeClass('error')
                } else {
                    $('.label-code').html('<div style="color: #dc3545;">Referral code does not exists.</div>')
                    $('#referral_code').addClass('error').removeClass('valid')
                }
            },
            error: function(err) {
                console.log(err)
            }
        });

        return res;
    });

    $('#birth_date').datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "1900:+0",
        onSelect: function() {
            $('#birth_date').removeClass('error');
            $('#birth_date + label').remove();
        },
        onClose: function() {
            $(this).trigger("focus").trigger("blur");
        }
    });

    $('#province, #city, #district, #village').select2({
        placeholder: 'Choose...',
    });

    $.ajax({
        type: 'GET',
        url: '{{ route('getPackets') }}',
        dataType: 'json',
        success: function(response) {
            if (response.message = 'success') {
                $.each(response.data, function(i, data) {
                    var el = $('<div class="custom-control custom-radio custom-control-inline"><input id="' + data.type + '" name="packets" type="radio" class="custom-control-input" value="' + data.id + '" data-price="' + data.price + '" data-error=".packets-error"><label class="custom-control-label" for="' + data.type + '">' + data.type + '</label></div>');
                    $('.member-type').append(el);
                });
            }

            $('input[name="packets"]').click(function() {
                if ($(this).is(':checked')) {
                    $('.price').html('<p>Rp. ' + $(this).attr('data-price') + '</p>');
                }
            });

            getProvinces();
        },
        error: function(err) {
            console.log(err);
        }
    });

    var province_id = 0;
    $('#province').on('change', function() {
        if (this.value != 0) {
            province_id = $(this).find(':selected').data('province-id');
            $.ajax({
                type: 'GET',
                url: '{{ route('getCities') }}?province_id=' + province_id,
                dataType: 'json',
                success: function(response) {
                    if (response.message = 'success') {
                        $('#city').find('option').remove().end();
                        $('#district').find('option').remove().end();
                        $('#village').find('option').remove().end();
                        $.each(response.data, function(i, data) {
                            $('#city').append('<option value="">Choose...</option>');
                            $('#city').append('<option value="' + data.name + '" data-city-id="' + data.id + '">' + data.name + '</option>');
                        });
                    }
                },
                error: function(err) {
                    console.log(err);
                }
            });
        }
    });

    var city_id = 0;
    $('#city').on('change', function() {
        if (this.value != 0) {
            city_id = $(this).find(':selected').data('city-id');
            $.ajax({
                type: 'GET',
                url: '{{ route('getDistricts') }}?city_id=' + city_id,
                dataType: 'json',
                success: function(response) {
                    if (response.message = 'success') {
                        $('#district').find('option').remove().end();
                        $('#village').find('option').remove().end();
                        $.each(response.data, function(i, data) {
                            $('#district').append('<option value="">Choose...</option>');
                            $('#district').append('<option value="' + data.name + '" data-district-id="' + data.id + '">' + data.name + '</option>');
                        });
                    }
                },
                error: function(err) {
                    console.log(err);
                }
            });
        }
    });

    var district_id = 0;
    $('#district').on('change', function() {
        if (this.value != 0) {
            district_id = $(this).find(':selected').data('district-id');
            $.ajax({
                type: 'GET',
                url: '{{ route('getVillages') }}?district_id=' + district_id,
                dataType: 'json',
                success: function(response) {
                    if (response.message = 'success') {
                        $('#village').find('option').remove().end();
                        $.each(response.data, function(i, data) {
                            $('#village').append('<option value="">Choose...</option>');
                            $('#village').append('<option value="' + data.name + '">' + data.name + '</option>');
                        });
                    }
                },
                error: function(err) {
                    console.log(err);
                }
            });
        }
    });
});
</script>
@endpush
