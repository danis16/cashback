@extends('layouts.auth')

@section('title')
<title>Cashback - Login</title>
@endsection

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style type="text/css">
.error {
    color: #dc3545;
}

.form-control:focus {
    border-color: #007bff  ;
    box-shadow: inset 0 1px 1px rgba(40, 167, 69, 0.25), 0 0 8px rgba(40, 167, 69, 0.6);
}

.custom-checkbox .custom-control-input:checked~.custom-control-label::before,
.custom-checkbox .custom-control-input:checked~.custom-control-label::after {
    background-color: #007bff  ;
    border-radius: 30%;
}

.custom-checkbox .custom-control-input:focus~.custom-control-label::before {
    box-shadow: 0 0 0 1px #fff, 0 0 0 0.2rem rgba(40, 167, 69, 0.25);
}
</style>
@endpush

@section('content')
{{-- <section class="fdb-block" style="background-image: url(https://images.unsplash.com/photo-1422651355218-53453822ebb8?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=536e9b7fd998734a42d6d199d79a5168&auto=format&fit=crop&q=100)"> --}}
<section class="fdb-block" style="background-image :url('{{asset('/img/background1.jpg')}}')">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8 col-lg-7 col-xl-5 text-center">
                <div class="fdb-box fdb-touch" style="border-top: solid 5px #007bff  ;">
                    <div class="row">
                        <div class="col">
                            <h1>Portal Member</h1>
                        </div>
                    </div>
                    <form method="post" id="formlogin" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf
                        <div class="row mt-4">
                            <div class="col">
                                <input name="email" id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col">
                                <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col">

                                {{-- <div class="input-group mb-3"> --}}
                                    <div class="input-group-prepend">
                                        {{-- <div class="input-group-text"> --}}
                                            <label for=""><input type="checkbox" aria-label="Checkbox for following text input" > </label>
                                            <label for="remember">&nbsp;&nbsp; Remember Me </label>
                                            {{-- </div> --}}
                                        </div>
                            </div>
                                        {{-- <input type="text" class="form-control" aria-label="Text input with checkbox"> --}}
                                      {{-- </div> --}}
                            {{-- <div class="col">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div> --}}
                        </div>
                        <div class="row mt-4">
                            <div class="col">
                                <button class="btn btn-primary" type="submit">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



</section>
@endsection

@push('scripts')

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/additional-methods.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script>

    $(document).ready(function() {

    $("#formlogin").validate({
        ignore: [],
        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: "{{url('cek_email_aktif')}}",
                    type: "get"
                        }
                    }

                },
            messages: {
            email:  {
                required: "Email tidak boleh kosong",
                remote: "Email belum diverifikasi"
                    }
                }
        });

    });
    </script>
@endpush
