@extends('layouts.auth')

@section('title')
<title>Cashback - Home</title>
@endsection

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ url('hierarchytree/main.css')}}">
<link rel="stylesheet" href="{{ url('hierarchytree/style.css')}}">
{{-- <link rel="stylesheet" href="{{ url('css/normalize.css')}}"> --}}

@endpush

@section('content')
<div class="container" style="margin-top : 10px; margin-bottom:50px;">
    <div class="row justify-content-center" style="border-top: solid 5px #007bff;">
        <div class="col-md-8" style="padding-top:50px">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
