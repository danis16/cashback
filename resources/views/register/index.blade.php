<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <title>Register | Cashback</title>

    <style type="text/css">
      .py-5 {
        padding-top: 5rem !important;
        padding-bottom: 5rem !important;
      }

      .error {
        color: #dc3545;
      }
    </style>
  </head>
  <body class="bg-light">
    <header>
      <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="{{ url('/') }}">Cashback</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="{{ route('login') }}">Login <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('register.index') }}">Sign Up</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <div class="container">
      <div class="py-5 text-center">
        <h2>Create a Cashback Account</h2>
        <p class="lead">Already have an account? Log in</p>
      </div>

      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="register" id="form-register" method="post" action="{{ route('register.store') }}">
            @csrf
            <h4 class="mb-3">Personal Information</h4>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="first_name">First Name<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="first_name" name="first_name">
              </div>
              <div class="col-md-6 mb-3">
                <label for="last_name">Last Name<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="last_name" name="last_name">
              </div>
            </div>

            <div class="mb-3">
              <label for="ktp">No KTP<span class="text-danger">*</span></label>
              <input type="text" class="form-control" id="ktp" name="ktp" maxlength="16">
            </div>

            <div class="mb-3">
              <label for="email">Email<span class="text-danger">*</span></label>
              <input type="email" class="form-control" id="email" name="email">
            </div>

            <div class="mb-3">
              <label for="gender">Gender<span class="text-danger">*</span></label>
              <div class="d-block">
                <div class="custom-control custom-radio custom-control-inline">
                  <input id="male" name="gender" type="radio" class="custom-control-input" value="M" data-error=".gender-error">
                  <label class="custom-control-label" for="male">Male</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input id="female" name="gender" type="radio" class="custom-control-input" value="F" data-error=".gender-error">
                  <label class="custom-control-label" for="female">Female</label>
                </div>
              </div>
              <div class="gender-error"></div>
            </div>

            <div class="mb-3">
              <label for="birth_date">Date of Birth<span class="text-danger">*</span></label>
              <input type="text" class="form-control" id="birth_date" name="birth_date" placeholder="YYYY-MM-DD">
            </div>

            <div class="mb-3">
              <label for="phone">Mobile Phone No<span class="text-danger">*</span></label><br>
              <input type="tel" class="form-control" id="phone" name="phone" maxlength="13">
            </div>
            
            {{-- <div class="row">
              <div class="col-12 col-sm-6 mb-3">
                <label for="province">Province<span class="text-danger">*</span></label>
                <select class="form-control" id="province" name="province">
                  <option value="">Choose...</option>
                </select>
              </div>

              <div class="col-12 col-sm-6 mb-3">
                <label for="city">City<span class="text-danger">*</span></label>
                <select class="form-control" id="city" name="city">
                </select>
              </div>
            </div>

            <div class="row">
              <div class="col-12 col-sm-6 mb-3">
                <label for="district">District<span class="text-danger">*</span></label>
                <select class="form-control" id="district" name="district">
                </select>
              </div>

              <div class="col-12 col-sm-6 mb-3">
                <label for="village">Village<span class="text-danger">*</span></label>
                <select class="form-control" id="village" name="village">
                </select>
              </div>
            </div> --}}

            <div class="mb-3">
              <label for="address">Address<span class="text-danger">*</span></label>
              <textarea class="form-control" id="address" name="address" rows="3"></textarea>
            </div>

            {{-- <div class="mb-3">
              <label for="company">Landline House / Company</label>
              <input type="text" class="form-control" id="company" name="company">
            </div> --}}

            <hr class="mb-4">

            <div class="mb-3">
              <label for="packets">Member Type<span class="text-danger">*</span></label>
              <div class="d-block member-type"></div>
              <div class="price"></div>
              <div class="packets-error"></div>
            </div>

            {{-- <hr class="mb-4">

            <h4 class="mb-3">Bank Account Information</h4>

            <div class="mb-3">
              <label for="bank_name">Bank Name<span class="text-danger">*</span></label>
              <select class="custom-select d-block w-100" id="bank_name" name="bank_name">
                <option value="">Choose...</option>
                <option value="BCA">BCA</option>
                <option value="BNI">BNI</option>
                <option value="BRI">BRI</option>
                <option value="Mandiri">Mandiri</option>
              </select>
            </div>

            <div class="mb-3">
              <label for="account_number">Account Number<span class="text-danger">*</span></label>
              <input type="text" class="form-control" id="account_number" name="account_number">
            </div>

            <div class="mb-3">
              <label for="account_name">Account Name<span class="text-danger">*</span></label>
              <input type="text" class="form-control" id="account_name" name="account_name">
            </div> --}}

            <div class="mb-3">
              <label for="referral_code">Referral Code</label>
              <input type="text" class="form-control" id="referral_code" name="referral_code">
            </div>

            <hr class="mb-4">

            <div class="mb-3">
              <label for="password">Password<span class="text-danger">*</span></label>
              <input type="password" class="form-control" id="password" name="password">
            </div>

            <div class="mb-3">
              <label for="confirm_password">Confirm Password<span class="text-danger">*</span></label>
              <input type="password" class="form-control" id="confirm_password" name="confirm_password">
            </div>

            <hr class="mb-4">

            <div class="mb-3">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="terms" name="terms" data-error=".terms-error">
                <label class="custom-control-label" for="terms">I agree to Terms & Condition and Privacy Policy.</label>
              </div>
              <div class="terms-error"></div>
            </div>

            <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>
          </form>
        </div>
        <div class="col-md-2"></div>
      </div>

      <footer class="my-5 pt-3 text-muted text-center text-small">
        <p class="mb-1">© 2018 Cashback</p>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="https://getbootstrap.com/docs/4.1/examples/checkout/?#">Privacy</a></li>
          <li class="list-inline-item"><a href="https://getbootstrap.com/docs/4.1/examples/checkout/?#">Terms</a></li>
          <li class="list-inline-item"><a href="https://getbootstrap.com/docs/4.1/examples/checkout/?#">Support</a></li>
        </ul>
      </footer>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/additional-methods.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
      function getPackets() {
        $.ajax({
          type: 'GET',
          url: '{{ route('getPackets') }}',
          dataType: 'json',
          success: function(response) {
            if (response.message = 'success') {
              $.each(response.data, function(i, data) {
                var el = $('<div class="custom-control custom-radio custom-control-inline"><input id="' + data.type + '" name="packets" type="radio" class="custom-control-input" value="' + data.id + '" data-price="' + data.price + '" data-error=".packets-error"><label class="custom-control-label" for="' + data.type + '">' + data.type + '</label></div>');
                $('.member-type').append(el);
              });
            }

            $('input[name="packets"]').click(function(){
              if ($(this).is(':checked')) {
                $('.price').html('<p>Rp. '+ $(this).attr('data-price') + '</p>');
              }
            });
          },
          error: function(err) {
            console.log(err);
          }
        });
      }

      $(document).ready(function() {
        $("#form-register").validate({
          ignore: [],
          rules: {
            first_name: {
              required: true
            },
            last_name: {
              required: true
            },
            ktp: {
              required: true,
              digits: true,
              minlength: 16
            },
            email: {
              required: true,
              email: true
            },
            gender: {
              required: true
            },
            birth_date: {
              required: true
            },
            phone: {
              required: true,
              digits: true
            },
            address: {
              required: true
            },
            packets: {
              required: true
            },
            referral_code: {
              remote: "{{ route('check.referral_code') }}"
            },
            password: {
              required: true
            },
            confirm_password: {
              required: true,
              equalTo: '#password'
            },
            terms: {
              required: true
            }
          },
          messages: {
            referral_code: {
              remote: "Referral code does not exists."
            }
          },
          errorElement: 'div',
          errorPlacement: function(error, element) {
              var placement = $(element).data('error');
              if (placement) {
                  $(placement).append(error)
              } else {
                  error.insertAfter(element);
              }
          }
        });

        $('#birth_date').datepicker({
          dateFormat: 'yy-mm-dd',
          changeMonth: true,
          changeYear: true,
          yearRange: "1900:+0",
          onSelect: function() {
            $('#birth_date').removeClass('error');
            $('#birth_date + label').remove();
          },
          onClose: function() {
            $(this).trigger("focus").trigger("blur");
          }
        });

        $('#province, #city, #district, #village').select2({
          placeholder: 'Choose...',
        });

        $.ajax({
          type: 'GET',
          url: '{{ route('getProvinces') }}',
          dataType: 'json',
          success: function(response) {
            if (response.message = 'success') {
              $.each(response.data, function(i, data) {
                $('#province').append('<option value="' + data.name + '" data-province-id="' + data.id + '">' + data.name + '</option>');
              });
            }

            getPackets();
          },
          error: function(err) {
            console.log(err);
          }
        });

        var province_id = 0;
        $('#province').on('change', function() {
          if (this.value != 0) {
            province_id = $(this).find(':selected').data('province-id');
            $.ajax({
              type: 'GET',
              url: '{{ route('getCities') }}?province_id=' + province_id,
              dataType: 'json',
              success: function(response) {
                if (response.message = 'success') {
                  $('#city').find('option').remove().end();
                  $('#district').find('option').remove().end();
                  $('#village').find('option').remove().end();
                  $.each(response.data, function(i, data) {
                    $('#city').append('<option value="">Choose...</option>');
                    $('#city').append('<option value="' + data.name + '" data-city-id="' + data.id + '">' + data.name + '</option>');
                  });
                }
              },
              error: function(err) {
                console.log(err);
              }
            });
          }
        });

        var city_id = 0;
        $('#city').on('change', function() {
          if (this.value != 0) {
            city_id = $(this).find(':selected').data('city-id');
            $.ajax({
              type: 'GET',
              url: '{{ route('getDistricts') }}?city_id=' + city_id,
              dataType: 'json',
              success: function(response) {
                if (response.message = 'success') {
                  $('#district').find('option').remove().end();
                  $('#village').find('option').remove().end();
                  $.each(response.data, function(i, data) {
                    $('#district').append('<option value="">Choose...</option>');
                    $('#district').append('<option value="' + data.name + '" data-district-id="' + data.id + '">' + data.name + '</option>');
                  });
                }
              },
              error: function(err) {
                console.log(err);
              }
            });
          }
        });

        var district_id = 0;
        $('#district').on('change', function() {
          if (this.value != 0) {
            district_id = $(this).find(':selected').data('district-id');
            $.ajax({
              type: 'GET',
              url: '{{ route('getVillages') }}?district_id=' + district_id,
              dataType: 'json',
              success: function(response) {
                if (response.message = 'success') {
                  $('#village').find('option').remove().end();
                  $.each(response.data, function(i, data) {
                    $('#village').append('<option value="">Choose...</option>');
                    $('#village').append('<option value="' + data.name + '">' + data.name + '</option>');
                  });
                }
              },
              error: function(err) {
                console.log(err);
              }
            });
          }
        });
      });
    </script>
  </body>
</html>