@extends('layouts.auth')
@section('title')
<title>Cashback - Check Out</title>
@endsection

@push('styles')
    <style>
    .header{
        background-color: #007bff;
        /* height: 10%; */
        width: 100%;
        color: white;
        text-align: center;
        padding-top: 2%;
        padding-bottom: 2%;
        font-family: Arial, Helvetica, sans-serif;
        margin:none;
    }
    .judul{
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        text-align: center;
        padding: 10px 0px 10px 0px;
    }
    .container{
        padding: 0px 50px 30px 50px;

    }
    td {
        padding-right: 20px;

    }
    .info-pembayaran{
        font-weight: 700;
    }
    .bank {
        padding: 5px;
        height: 100%;
    }
    .paragraf {
        float: none;
    }
    .bank-container{
        padding: 20px 25px;

    }

    @media screen and (min-width:770px){
        .bank-container {
            float: left;
        }
    }


    @media screen and (max-width:769px){
        .bank-container {
            float: none;
        }
    }
    </style>
    @endpush
{{-- </head>
<body> --}}
        @section('content')
    <div class="garis" style="border-top: solid 5px #007bff;">
<br>
    </div>
    <div class="container">
        <div class="judul"><h3>INFO PEMBAYARAN</h3></div>
        <div class="paragraf">
            <p> Halo {{$customer->first_name}} {{$customer->last_name}} </p>

            <table class="info-pembayaran">
                <tr>
                    <td>NO PEMBAYARAN</td>
                    <td>: {{$user->id}}</td>
                </tr>
                <tr>
                    <td>INFO PAKET</td>
                    <td>: {{$packets->type}}</td>
                </tr>
                <tr>
                    <td>TOTAL BIAYA</td>
                    <td>: {{$packets->price}}</td>
                </tr>
               </table>
               <br>


            <p>Untuk melakukan  pembayaran bisa di lakukan di rekening ini :</p>
        </div>
        <div class="paragraf">
            <div class="bank">
                <div class="bank-container">
                    <img src="{{ asset('img/bni.png') }}" width="100px" alt="bni" height="40px">
                    <p></p>
                    <p>Danang Listyanto<br>1111-1111-1111</p>

                </div>

                <div class="bank-container">
                    <img src="{{ asset('img/bca.png') }}" width="100px" alt="bni" height="40px">
                    <p></p>
                    <p>Danang Setiabudi <br> 1111-1111-1111</p>
                </div>

                <div class="bank-container">
                <img src="{{ asset('img/mandiri.png') }}" width="100px" alt="bni" height="40px">
                <p></p>
                <p>Danang Sudanang<br>1111-1111-1111</p>
            </div>
            </div>
        </div>
        <br><br><br><br><br><br><br>
        <div class="paragraf">

            <p>THIS IS REGISTRATION PAYMENT. THANK YOU FOR CHOOSING US AS YOUR PARTNER.</p>
            <p>Jika sudah melakukan pembayaran paket Anda, silakan login akun Anda pada Website CashBack</p>


        </div>
        <div class="judul">Terima kasih telah bergabung di CASHBACK</div>
            <div class="paragraf">
                    <p>Untuk informasi lebih lanjut silakan hubungi kami : </p>
                <table style="padding:10px">
                    <tr>
                        <td>Telp</td>
                        <td>: [021-xxxx-xxxx]</td>
                    </tr>
                    <tr>
                        <td>Jam kerja</td>
                        <td>: Senin – Jumat, pukul 08.00 – 18.00</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>: cashbacksolutech@gmail.com</td>
                    </tr>
                </table>


        </div>

        </div>
    </div>

@endsection
{{-- </body>
</html> --}}
