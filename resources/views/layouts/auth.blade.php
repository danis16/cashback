<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  @yield('title')

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/froala-design-blocks/1.0.2/css/froala_blocks.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

  @stack('styles')
</head>
@if(\Route::currentRouteName() == 'member.structure')
    {{-- <body onload="initialize()"> --}}
    <body onload="">
@else
    <body>
@endif
<header>
  <div class="container">
    <nav class="navbar navbar-expand-md no-gutters">
      <div class="col-3 text-left">
            <img src="{{ asset('img/cashback_logo.png') }}" width="100" alt="image" class="img-fluid">
          </a>
      </div>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse-2" aria-controls="navbarNav7" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse navbar-collapse-2 justify-content-center col-md-6" id="navbarNav7">
            <ul class="navbar-nav justify-content-center">
                @auth
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('member.structure') }}">My Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('member.structure') }}">Bonus</a>
                    </li>
                @endauth
            </ul>
        </div>

      <div class="collapse navbar-collapse navbar-collapse-2">
        <ul class="navbar-nav ml-auto justify-content-end">
          <li class="nav-item">
            @if (\Auth::user())
                 <a class="btn btn-primary ml-md-3" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @else
                 {{-- <a class="btn btn-primary ml-md-3" href="{{ route('register') }}">Register</a>  --}}
                 <a class="btn btn-primary ml-md-3" href="{{ route('payment.confirmation.index') }}">Konfirmasi Pembayaran</a>

            @endif
          </li>
        </ul>

      </div>
    </nav>
  </div>
</header>

@yield('content')

<footer class="fdb-block footer-large bg-dark">
  <div class="container">
    <div class="row align-items-top text-center text-md-left">
      <div class="col-12 col-sm-6 col-md-4">
        <h3><strong>Cashback</strong></h3>
        <p>Street Address 52<br>Indonesia</p>
        <p>+44 827 312 5002</p>
        <p><a href="https://www.froala.com">support@cashback.com</a></p>
      </div>

      <div class="col-12 col-sm-6 col-md-4 mt-4 mt-sm-0">

      </div>

      <div class="col-12 col-md-4 mt-5 mt-md-0 text-md-left">
        <h3><strong>Terms and Condition</strong></h3>
        <p>Item 1</p>
        <p>Item 2</p>
        <p>Item 3</p>
      </div>
    </div>

    <div class="row mt-5">
      <div class="col text-center">
        © 2018 Cashback. All Rights Reserved.
      </div>
    </div>
  </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

@stack('scripts')

</body>
</html>
