@extends('layouts.auth')

@section('title')
<title>Cashback - Add Member</title>
@endsection

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
.error {
    color: #dc3545;
}

.form-control:focus {
    border-color: #007bff;
    box-shadow: inset 0 1px 1px rgba(40, 167, 69, 0.25), 0 0 8px rgba(40, 167, 69, 0.6);
}

.custom-radio .custom-control-input:checked~.custom-control-label::before,
.custom-radio .custom-control-input:checked~.custom-control-label::after {
    background-color: #007bff;
    border-radius: 50%;
}

.custom-radio .custom-control-input:focus~.custom-control-label::before {
    box-shadow: 0 0 0 1px #fff, 0 0 0 0.2rem rgba(40, 167, 69, 0.25);
}

.custom-checkbox .custom-control-input:checked~.custom-control-label::before,
.custom-checkbox .custom-control-input:checked~.custom-control-label::after {
    background-color: #007bff;
    border-radius: 30%;
}

.custom-checkbox .custom-control-input:focus~.custom-control-label::before {
    box-shadow: 0 0 0 1px #fff, 0 0 0 0.2rem rgba(40, 167, 69, 0.25);
}
</style>
@endpush

@section('content')
    <div class="container" style="margin-top : 10px; margin-bottom:50px;">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="fdb-box fdb-touch" style="border-top: solid 5px #007bff;">
                    <div class="row">
                        <div class="col text-center">
                            <h1 style="text-align: center; margin-left:20px; margin-top:20px;">Add Member</h1>
                        </div>
                    </div>
                    @if(session()->has('alert'))
                    <div class="row">
                        <div class="alert alert-danger" role="alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {!! session('alert') !!}
                        </div>
                    </div>
                    @endif
                    <div class="row justify-content-center">
                        <form class="member" id="formmember" method="post" action="{{ route('member.storeMember') }}" aria-label="{{ __('Register') }}">
                        {{-- <form class="member" id="formmember" method="post" action="{{ route('send.email') }}" aria-label="{{ __('Register') }}"> --}}
                            @csrf
                            <hr class="mb-4">
                            <div class="mb-3">
                                <label for="referral_code">Your Referral Code</label>

                                <input type="text" class="form-control" id="referral_code" name="referral_code" value="@if (isset($customer->referral_code)) {{$customer->referral_code}} @endif" readonly >
                                <div class="label-code"></div>
                            </div>
                            <div class="mb-3">


                                <label for="customer_code">Reference Code <span class="text-danger">*</span></label>
                                <input type="text" name="customer_code" value="{{$customer_code}}" class="form-control" readonly >
                                <input type="hidden" name="user_groups" value="{{$user_groups}}" class="form-control" readonly >
                                {{-- <input type="text" class="form-control" id="customer_code" name="customer_code"> --}}
                                {{-- <select class="form-control" id="customer_code" name="customer_code" onchange="referal_code()"> --}}
                                {{-- <select class="form-control" id="customer_code" name="customer_code" > --}}

                                    {{-- @foreach ($select_position as $key => $value) --}}
                                {{-- <option value="{{$value->customer_code}}">{{$key+1}}</option> --}}
                                {{-- @endforeach --}}
                                {{-- <option value="2">2</option> --}}

                                {{-- {{-- <option value="{{$select_position['0']}}">1</option> --}}


                                {{-- <option value="{{$select_position_max->customer_code}}">2</option> --}}


                                {{-- </select> --}}
                                {{-- <input type="hidden" name="ref_generate" id="ref_generate" value="aaa">                                 --}}
                                <div class="label-code"></div>
                            </div>

                            <script>
                                // function referal_code(){
                                //     var cust_code = document.getElementById("customer_code").value;
                                //     $.ajax({
                                //                 url:"{{url('member/ref_code')}}",
                                //                 data:"cust_code="+cust_code,
                                //                 success: function (html) {
                                //             load();
                                //         }
                                //     })
                                // }

                                // function load(){
                                //     // alert($_GET['cust_code']);
                                //     document.getElementById('referral_code').value = document.getElementById('customer_code').value;
                                //     alert('load sukses');
                                // }
                            </script>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="first_name">First Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="last_name">Last Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="ktp">ID Number ( KTP ) <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="ktp" name="ktp" maxlength="16" placeholder="ID Number">
                            </div>
                            <div class="mb-3">
                                <label for="birth_date">Birthdate<span class="text-danger">*</span></label>
                                <input type="date" class="form-control" name="birth_date" placeholder="DD-MM-YYYY">
                            </div>
                            <div class="mb-3">
                                <label for="gender">Gender<span class="text-danger">*</span></label>
                                <div class="d-block">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input id="male" name="gender" type="radio" class="custom-control-input" value="M" data-error=".gender-error" checked>
                                        <label class="custom-control-label" for="male">Male</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input id="female" name="gender" type="radio" class="custom-control-input" value="F" data-error=".gender-error">
                                        <label class="custom-control-label" for="female">Female</label>
                                    </div>
                                </div>
                                <div class="gender-error"></div>
                            </div>
                            <div class="mb-3">
                                <label for="phone">Mobile<span class="text-danger">*</span></label><br>
                                <input type="tel" class="form-control" id="phone" name="phone" maxlength="13" placeholder="Mobile">
                            </div>
                            <div class="mb-3">
                                <label for="address">Address<span class="text-danger">*</span></label>
                                <textarea class="form-control" id="address" name="address" rows="3" placeholder="Address"></textarea>
                            </div>
                            <div class="mb-3">
                                <label for="email">Email<span class="text-danger">*</span></label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                {{-- <input type="hidden" name="user_groups" value="{{$user_groups}}"> --}}
                                {{-- <input type="hidden" name="user_groups" value="{{$user_groups}}"> --}}
                            </div>
                            {{-- <div class="mb-3">
                                <label for="password">Password<span class="text-danger">*</span></label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                            <div class="mb-3">
                                <label for="confirm_password">Confirm Password<span class="text-danger">*</span></label>
                                <input type="password" class="form-control" id="confirm_password" name="confirm_password">
                            </div> --}}
                            <hr class="mb-4">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/additional-methods.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#formmember").validate({
        ignore: [],
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            // customer_code: {
            //     required: true
            // },
            ktp: {
                required:true,
                digits: true,
                minlength: 16,
                remote: {
                    url: "{{url('member/cek_ktp')}}",
                    type: "get"
                }


            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: "{{url('member/cek_email')}}",
                    type: "get"
                }
            },
            gender: {
                required: true
            },
            birth_date: {
                required: true
            },
            phone: {
                required: true,
                digits: true
            },
            address: {
                required: true
            },
            password: {
                required: true
            },
            confirm_password: {
                required: true,
                equalTo: '#password'
            },

        },
        messages: {
            ktp: {
                remote: "KTP already exists."
            },
            email: {
                remote: "Email already exists."
            }
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#birth_date').datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "1900:+0",
        onSelect: function() {
            $('#birth_date').removeClass('error');
            $('#birth_date + label').remove();
        },
        onClose: function() {
            $(this).trigger("focus").trigger("blur");
        }
    });

});
</script>
@endpush
