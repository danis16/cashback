@extends('layouts.auth')

@section('title')
<title>Cashback - Member Structure</title>
@endsection

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{ url('hierarchytree/main.css')}}">
<link rel="stylesheet" href="{{ url('hierarchytree/style.css')}}">
<link rel="stylesheet" href="{{ url('css/normalize.css')}}">

@endpush

@section('content')
<div class="container" style="margin-top : 10px; margin-bottom:50px;">
    <div class="row">
        <div class="col-12 ">

            <div class="fdb-box fdb-touch" style="border-top: solid 5px #007bff;">
                @if(session('message'))
                <div class="row">
                    <div class="alert alert-{{ session('status') }}" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {!! session('message') !!}
                    </div>
                </div>
                @endif
                <div class="row" style="padding-right:15px; padding-left:15px;">
                    <div class="header-kanan">
                        <h1 style="text-align: left; margin-left:20px; margin-top:20px;">My Team  </h1>
                    </div>
                    <div class="header-kiri">
                        @if ($id_user==1)
                        <button class = "btn btn-primary btn-sm" style="margin-right:0px; margin-top:20px; float:right;" onclick="relocateAddMember()">Add Member</button>

                        @endif



                        <br><br><br>
                    {{-- goto member  --}}
                        {{-- <label style="margin-right:0px; margin-top:10px; float:left;" > Go to Member No  </label>
                        <button class="btn btn-primary" style="width:40px; float:right; margin-left:10px;"> Go </button>
                        <input type="text" class="form-control" style="width:40px; float:right;" name="inputan_go"><br><br> --}}
                    {{-- total       --}}
                        {{-- <label style="margin-right:160px; margin-top:10px;float:left;" for=""> <strong>Total  </strong> </label>
                        <label style="margin-right:0px; margin-top:10px; float:right;" for=""> <strong> : {{$count_table}} </strong> </label> <br> --}}

                    </div>

                </div>
            </div>


            <div id="control_panel"></div>
            {{-- <div id="divider">
            </div> --}}

            <?php
            function hierarchytree($member) {
                if(isset($member))
                {
                    if($member->getChild() != '[]'){
                        //warna abu-abu
                        if (empty($member->getChild()[0]->first_name)) {
                            $kosong = 'style="background:#c9c2c2"';
                        } else {
                            $kosong = '';
                        }

                        if (empty($member->getChild()[1]->first_name)) {
                            $kosongg = 'style="background:#c9c2c2"';
                        } else {
                            $kosongg = '';
                        }

                        // buat link

                        if ($member->getChild()[0]->users->active==1) {
                            $link = url('member/structure_one').'/'.$member->getChild()[0]->user_id;

                        } else {
                                if ($member->getChild()[0]->groups == 4) {
                                    $link = url('member/create').'/'.$member->getChild()[0]->customer_code.'/group/'.$member->getChild()[0]->user_groups;
                                } else {
                                    $link = '#';
                                }

                            // }
                        }

                        if ($member->getChild()[1]->users->active==1) {
                            $linkk = url('member/structure_one').'/'.$member->getChild()[1]->user_id;
                        } else {


                                if ($member->getChild()[1]->groups == 4) {
                                    $linkk = url('member/create').'/'.$member->getChild()[1]->customer_code.'/group/'.$member->getChild()[1]->user_groups;
                                } else {
                                    $linkk = '#';
                                }


                            // }
                        }


                        //  Buat tampilin nama

                        if ($member->getChild()[0]->users->active==1){
                             $tampil =$member->getChild()[0]->identity.' '.$member->getChild()[0]->customer_code.' '.$member->getChild()[0]->groups.' '.$member->getChild()[0]->user_groups.' '.$member->getChild()[0]->first_name.' '.$member->getChild()[0]->last_name;
                            // $tampil =$member->getChild()[0]->first_name.' '.$member->getChild()[0]->last_name;
                        } else {
                            $tampil = $member->getChild()[0]->identity.' '.$member->getChild()[0]->customer_code.' '.$member->getChild()[0]->groups.' '.$member->getChild()[0]->user_groups.' Add';
                            // $tampil = ' Add';
                        }

                        if ($member->getChild()[1]->users->active==1){
                            $tampill =$member->getChild()[1]->identity.' '.$member->getChild()[1]->customer_code.' '.$member->getChild()[1]->groups.' '.$member->getChild()[1]->user_groups.' '.$member->getChild()[1]->first_name.' '.$member->getChild()[1]->last_name;
                            // $tampill =$member->getChild()[1]->first_name.' '.$member->getChild()[1]->last_name;
                        } else {
                            $tampill = $member->getChild()[1]->identity.' '.$member->getChild()[1]->customer_code.' '.$member->getChild()[1]->groups.' '.$member->getChild()[1]->user_groups.' Add';
                            // $tampill = ' Add';
                        }

                        echo    '<ul>
                                    <li><a '.$kosong.' href='.$link.' id='.$member->getChild()[0]->user_id.'>'.$tampil.'</a>';
                                        ?> {{hierarchytree($member->getChild()[0])}} <?php
                                    echo '</li>';
                                    if(count($member->getChild()) > 1){
                                        echo '<li><a '.$kosongg.' href='.$linkk.' id='.$member->getChild()[1]->user_id.'>'.$tampill.' </a>';
                                            ?> {{hierarchytree($member->getChild()[1])}} <?php
                                        echo '</li>';
                                    }
                        echo '</ul>';
                    }
                }

             } ?>

            <div class="tree" style="padding:50px">

                    <ul>
                            <li>
                                @if(isset($first_member))
                                    <a href="#"> {{$first_member->identity}} {{$first_member->customer_code}} {{$first_member->groups}} {{$first_member->user_groups}} {{$first_member->first_name}} {{$first_member->last_name}}</a>
                                    {{-- <a href="#">  {{$first_member->first_name}} {{$first_member->last_name}}</a> --}}
                                @else
                                    <a href="#"></a>
                                @endif

                            {{hierarchytree($first_member)}}

                            </li>
                        </ul>

              </div>



            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/additional-methods.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ url('hierarchytree/main.js') }}"></script>
<script src="{{ url('hierarchytree/tree.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
    function relocateAddMember()
    {
        window.location.href = "{{ route('member.create') }}";
    }

    var tree = "";


</script>
@endpush
