@extends('layouts.auth')

@section('title')
<title>Cashback</title>
@endsection

@section('content')
<section class="fdb-block" id="home">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-md-6 mb-4 mb-md-0">
        <img alt="image" class="img-fluid" src="https://images.unsplash.com/photo-1538356596070-887d2c0b0d63?ixlib=rb-0.3.5&s=903073ac96937d700b19cca1cfdcee53&auto=format&fit=crop&w=500&q=60">
      </div>
      <div class="col-12 col-md-6 col-lg-5 ml-md-auto text-left">
        <h1>Cashback</h1>
        <p class="text-h3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum omnis magni maiores mollitia, molestiae rem similique pariatur eligendi amet placeat ipsam rerum nobis dolorem at reprehenderit voluptas modi nostrum fuga.</p>
        <p><a class="btn btn-round btn-success mt-4" href="{{ route('register') }}">Get Started</a></p>
      </div>
    </div>
  </div>
</section>
<section class="fdb-block fp-active" id="features">
  <div class="container">
    <div class="row justify-content-center pb-5">
      <div class="col-12 text-center">
        <h1>Features</h1>
      </div>
    </div>

    <div class="row text-left align-items-center pt-5 pb-md-5">
      <div class="col-4 col-md-5">
        <img alt="image" class="img-fluid" src="https://images.unsplash.com/photo-1530041859951-6cbea5e69b14?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a40881b3b4e88c81dda3f8ef8d9c32aa&auto=format&fit=crop&q=100">
      </div>

      <div class="col-12 col-md-5 m-md-auto">
        <h2><strong>Feature One</strong></h2>
        <p class="text-h3">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
        <p><a href="https://www.froala.com">Learn More &gt;</a></p>
      </div>
    </div>

    <div class="row text-left align-items-center pt-5 pb-md-5">
      <div class="col-4 col-md-5 m-md-auto order-md-5">
        <img alt="image" class="img-fluid" src="https://images.unsplash.com/photo-1530042133068-0296a09adf7b?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=05d2e861ce42fc2fdc790d5b44d9d06e&auto=format&fit=crop&q=100">
      </div>

      <div class="col-12 col-md-5">
        <h2><strong>Feature Two</strong></h2>
        <p class="text-h3">Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
        <p><a href="https://www.froala.com">Learn More &gt;</a></p>
      </div>
    </div>

    <div class="row text-left align-items-center pt-5">
      <div class="col-4 col-md-5">
        <img alt="image" class="img-fluid" src="https://images.unsplash.com/photo-1521106452064-182f55b938cc?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=4e419c0d7f52a71be85d49092c956e44&auto=format&fit=crop&q=100">
      </div>

      <div class="col-12 col-md-5 m-md-auto">
        <h2><strong>Feature Three</strong></h2>
        <p class="text-h3">On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn
          around.</p>
        <p><a href="https://www.froala.com">Learn More &gt;</a></p>
      </div>
    </div>
  </div>
</section>
<section class="fdb-block bg-dark" style="background-image: url(https://images.unsplash.com/photo-1521798948753-02f843fd3e5d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=7a2059a47213b313bb76f8996273d794&auto=format&fit=crop&q=100);" id="pricing">
  <div class="container">
    <div class="row text-center">
      <div class="col">
        <h1>Pricing Plans</h1>
      </div>
    </div>

    <div class="row mt-5 align-items-center no-gutters">
      <div class="col-12 col-sm-10 col-md-8 m-auto col-lg-4 text-center">
        <div class="bg-gray pb-5 pt-5 pl-4 pr-4">
          <h2 class="font-weight-light">Basic</h2>

          <p class="text-h1 mt-5 mb-5"><strong>Rp 500.000</strong></p>

          <ul class="text-left">
            <li>Item 1</li>
            <li>Item 2</li>
            <li>Item 3</li>
          </ul>

          <p class="text-center pt-4"><a href="{{ route('register') }}" class="btn btn-round btn-white btn-shadow">Choose Plan</a></p>
        </div>
      </div>

      <div class="col-12 col-sm-10 col-md-8 ml-auto mr-auto col-lg-4 text-center mt-4 mt-lg-0 sl-1 pt-0 pt-lg-3 pb-0 pb-lg-3 bg-gray fdb-touch" style="border-top: solid 5px #007bff;">
        <div class="pb-5 pt-5 pl-4 pr-4">
          <h2 class="font-weight-light">Gold</h2>

          <p class="text-h1 mt-5 mb-5"><strong>Rp 1.000.000</strong></p>

          <ul class="text-left">
            <li>Item 1</li>
            <li>Item 2</li>
            <li>Item 3</li>
          </ul>

          <p class="text-center pt-4"><a href="{{ route('register') }}" class="btn btn-primary btn-round btn-shadow">Choose Plan</a></p>
        </div>
      </div>

      <div class="col-12 col-sm-10 col-md-8 ml-auto mr-auto col-lg-4 text-center mt-4 mt-lg-0">
        <div class="bg-gray pb-5 pt-5 pl-4 pr-4">
          <h2 class="font-weight-light">Platinum</h2>

          <p class="text-h1 mt-5 mb-5"><strong>Rp 1.500.000</strong></p>

          <ul class="text-left">
            <li>Item 1</li>
            <li>Item 2</li>
            <li>Item 3</li>
          </ul>

          <p class="text-center pt-4"><a href="{{ route('register') }}" class="btn btn-round btn-white btn-shadow">Choose Plan</a></p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="fdb-block" id="team">
  <div class="container">
    <div class="row text-center justify-content-center">
      <div class="col-8">
        <h1>Meet Our Team</h1>
      </div>
    </div>

    <div class="row-50"></div>

    <div class="row text-center justify-content-center">
      <div class="col-sm-3 m-sm-auto">
        <img alt="image" class="img-fluid" src="https://images.unsplash.com/photo-1492546662075-aabebf46dee2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=fa31c57eafca4772d09602394cb0cb64&auto=format&fit=crop&q=100">

        <h2>John Doe</h2>
        <p>Founder</p>
      </div>

      <div class="col-sm-3 m-sm-auto">
        <img alt="image" class="img-fluid" src="https://images.unsplash.com/photo-1500521680613-a8f77c5cd0f5?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=d5e72141d8cda33ebb9e65e92089d629&auto=format&fit=crop&q=100">

        <h2>Sara Doe</h2>
        <p>Founder</p>
      </div>

      <div class="col-sm-3 m-sm-auto">
        <img alt="image" class="img-fluid" src="https://images.unsplash.com/photo-1492546662075-aabebf46dee2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=fa31c57eafca4772d09602394cb0cb64&auto=format&fit=crop&q=100">

        <h2>Jane Doe</h2>

        <p>Founder</p>
      </div>
    </div>
  </div>
</section>
<section class="fdb-block bg-gray" id="contact">
  <div class="container">
    <div class="row text-center justify-content-center">
      <div class="col-12 col-md-8 col-lg-7">
        <p class="text-h2">support@cashback.com</p>
        <p class="text-h3">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
        <p><br></p>
        <p class="text-h2">
          <a href="https://www.froala.com"><i class="fa fa-facebook text-success"></i></a>&nbsp;&nbsp;&nbsp;
          <a href="https://www.froala.com"><i class="fa fa-twitter text-success"></i></a>&nbsp;&nbsp;&nbsp;
          <a href="https://www.froala.com"><i class="fa fa-instagram text-success"></i></a>&nbsp;&nbsp;&nbsp;
          <a href="https://www.froala.com"><i class="fa fa-google text-success"></i></a>&nbsp;&nbsp;&nbsp;
          <a href="https://www.froala.com"><i class="fa fa-pinterest text-success"></i></a>
        </p>
      </div>
    </div>
  </div>
</section>
@endsection
