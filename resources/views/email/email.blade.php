<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    .header{
        background-color: #007bff;
        /* height: 10%; */
        width: 100%;
        color: white;
        text-align: center;
        padding-top: 2%;
        padding-bottom: 2%;
        font-family: Arial, Helvetica, sans-serif;
        margin:none;
    }
    .judul{
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        text-align: center;
        padding: 10px 0px 10px 0px;
    }
    .container{
        padding: 0px 30px 30px 30px;

    }
    td {
        padding-right: 20px;
    }
    </style>
</head>
<body>
    <div class="header"><h2>CashBack.Com</h2></div>
    <div class="container">
        <div class="judul"><h3>KONFIRMASI REGISTER</h3></div>
        <div class="paragraf">
            <p> Halo {{$nama}} </p>
            {{-- <p> <a href="{{ url('member/update_status_email')}}/{{$email}}" > Here to confirm your register </a> </p> --}}
            <p> <a href="{{ url('get_data_email')}}/{{$konvertToBase64}}" > Here to confirm your register </a> </p>
            <p>
                THIS IS REGISTRATION CONFIRMATION. THANK YOU FOR CHOOSING US AS YOUR PARTNER.
            </p>
            <p>Untuk melihat status pesanan Anda, silakan login akun Anda pada Website CashBack </p>

        <div class="judul"><h3>Terima kasih telah bergabung di CASHBACK</h3></div>


            <div class="paragraf">

                <p>Untuk informasi lebih lanjut silakan hubungi kami : </p>
                <table style="padding:10px">
                    <tr>
                        <td>Telp</td>
                        <td>: [021-xxxx-xxxx]</td>
                    </tr>
                    <tr>
                        <td>Jam kerja</td>
                        <td>: Senin – Jumat, pukul 08.00 – 18.00</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                    </tr>
                </table>
                <p>*Email ini dibuat secara otomatis. Mohon untuk tidak mengirimkan balasan ke email ini. </p>

        </div>


        </div>
    </div>

    <div class="header"><h2>CashBack.Com</h2></div>

</body>
</html>

{{--
<!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Verifikasi</title>
            <link type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        </head>
        <style type="text/css">
        tr.gradient:nth-child(odd) {
            background-color: #ffe6e6;
            font-size: small;
        }
        tr.gradient:nth-child(even) {
            background-color: #ffb3b3;
            font-size: small;
        }
        dt {
        font-weight: inherit;
        }

        .dl-horizontal dt {
        float: left;
        width: 140px;
        overflow: hidden;
        clear: left;
        text-align: right;
        text-overflow: ellipsis;
        white-space: nowrap;
        }
    </style>
        <body bgcolor="#8d8e90">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#8d8e90">
                <tr>
                    <td>
                        <table width="900" border="0" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" align="center">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <img src="" alt="">
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
     			<tr>
                                <td>
<table width="100%" border="0" cellspacing="0" cellpadding="15" style="font-family: Segoe UI;font-size: 18px;">

<style type="text/css">
    tr.gradient:nth-child(odd) {
        background-color: #ffe6e6;
        font-size: small;
    }
    tr.gradient:nth-child(even) {
        background-color: #ffb3b3;
        font-size: small;
    }
    .kupon {margin: auto;
        height: 70px;
        width: 200px;
        background: #eee;
        border: 1px solid #ccc;
    }
    .kupon p{margin: 10px auto 10px;
        font-size: 10px;
        text-align: center;
        font-weight: bold;

    }
    .kupon p span.abc{
        font-size: 18px;
        font-weight: bold;
        color: blue;
    }
</style>

<tr>
    <td style="padding-top: 0px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="15" style="font-family: Segoe UI;font-size: 18px;">
            <tr>
                <td align="left" valign="top"><font style="font-family: Verdana, Geneva, sans-serif; color:#000000; font-size:13px; line-height:21px"> <br>
                <p align="center" style="font-size: 20px;"> <strong>KONFIRMASI PENDAFTARAN</strong></p><br>
                Halo , {{ $nama }}<br>
                    Email ini digunakan untuk konfirmasi pendaftaran, silahkan klik link dibawah untuk konfirmasi pendaftaran.<br> --}}
                    {{-- <a href="{{$link}}">Klik Disini Untuk Konfirmasi Pendaftaran</a><br> --}}
                  <!-- Pendaftaran ini untuk Konfirmasi, Terima Kasih Telah Bergabung. <br>      -->

                  <!-- terima kasih telah bergabung dengan Winnerzz. <br>   -->
                  <!-- Untuk melihat daftar anda, silahkan ke login akun anda pada situs winnerz atau aplikasi winnerz. <br>  -->
    			{{-- </td>


            </tr>
            <tr>
                <td align="left" valign="top"><font style="font-family: Verdana, Geneva, sans-serif; color:#000000; font-size:13px; line-height:21px"> <br>
                    <p align="center" style="font-size: 18px;"> <strong>Terima kasih telah bergabung di WINNERZZ.</strong></p><br> --}}
                    {{-- <p>Untuk Informasi lebih lanjut silahkan <a href="{{route('member.update_status_email')}}">klik disini</a></p> --}}
                    {{-- <p>Untuk Informasi lebih lanjut silahkan <a href="{{ url('member/update_status_email')}}/{{$email}}">klik disini</a></p> --}}
                    {{-- <p>Telp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 081218924692 </p>
                    <p>Jam Kerja&nbsp;&nbsp;&nbsp;: Senin - Jumat, Pukul 08:00-18</p>
                    <p>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: info@winnerzz.net</p>
                     --}}
                {{-- </td>
            </tr>
        </table>
    </td>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Segoe UI;font-size: 18px;">
                                        <tr>
                                            <td align="center"><img src="" width="100%"></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Segoe UI;font-size: 12px;">*email ini dibuat secara otomatis, mohon untuk tidak mengirimkan balasan ke email ini.</td><br><br>

                                        </tr>
                                    </table> --}}
   {{-- <img src="{{url('public/images/footer.png')}}" width="100%"> --}}
                                {{-- </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
    </html> --}}
