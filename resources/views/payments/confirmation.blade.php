@extends('layouts.auth')

@section('title')
<title>Cashback - Add Member</title>
@endsection

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
.error {
    color: #dc3545;
}

.form-control:focus {
    border-color: #007bff;
    box-shadow: inset 0 1px 1px rgba(40, 167, 69, 0.25), 0 0 8px rgba(40, 167, 69, 0.6);
}

.custom-radio .custom-control-input:checked~.custom-control-label::before,
.custom-radio .custom-control-input:checked~.custom-control-label::after {
    background-color: #007bff;
    border-radius: 50%;
}

.custom-radio .custom-control-input:focus~.custom-control-label::before {
    box-shadow: 0 0 0 1px #fff, 0 0 0 0.2rem rgba(40, 167, 69, 0.25);
}

.custom-checkbox .custom-control-input:checked~.custom-control-label::before,
.custom-checkbox .custom-control-input:checked~.custom-control-label::after {
    background-color: #007bff;
    border-radius: 30%;
}

.custom-checkbox .custom-control-input:focus~.custom-control-label::before {
    box-shadow: 0 0 0 1px #fff, 0 0 0 0.2rem rgba(40, 167, 69, 0.25);
}
</style>
@endpush

@section('content')
    <div class="container" style="margin-top : 10px; margin-bottom:50px;">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="fdb-box fdb-touch" style="border-top: solid 5px #007bff;">
                    <div class="row">
                        <div class="col text-center">
                            <h1 style="text-align: center; margin-left:20px; margin-top:20px;">Formulir Konfirmasi Pembayaran</h1>
                        </div>
                    </div>
                    {{-- @if(session()->has('alert'))
                    <div class="row">
                        <div class="alert alert-danger" role="alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {!! session('alert') !!}
                        </div>
                    </div>
                    @endif --}}
                    <div class="row justify-content-center">
                        <form class="member" id="formmember" method="post" enctype="multipart/form-data" action="{{ route('payment.confirmation.create') }}" aria-label="{{ __('Register') }}">
                        {{-- <form class="member" id="formmember" method="post" action="{{ route('send.email') }}" aria-label="{{ __('Register') }}"> --}}
                            @csrf
                            <hr class="mb-4">

                            <div class="mb-3">
                                    <label for="referral_code">Nomor Pembayaran<span class="text-danger">*</span></label>

                                    <input type="text" class="form-control" id="user_id" name="user_id" placeholder="Nomor Pembayaran" >
                                    <div class="label-code"></div>
                            </div>

                            <div class="mb-3">
                                <label for="referral_code">Nama Sesuai Buku Tabungan<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" id="account_name" name="account_name"  placeholder="Nama Sesuai Buku Tabungan" >
                                <div class="label-code"></div>
                            </div>

                            <div class="mb-3">
                                <label for="packets">Member Type<span class="text-danger">*</span></label>
                                <div class="d-block member-type"></div>
                                <div class="price"></div>
                                <div class="packets-error"></div>
                            </div>

{{--
                            <div class="mb-3">
                                <label for="referral_code">Jumlah<span class="text-danger">*</span></label>

                                <input type="number" class="form-control" id="amount" name="amount" placeholder="Jumlah" >
                                <div class="label-code"></div>
                            </div> --}}

                            <div class="mb-3">
                                    <label for="payment_method">Payment Method<span class="text-danger">*</span></label>
                                    <div class="d-block">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input id="atm" name="payment_method_id" type="radio" class="custom-control-input" value="1" data-error=".payment_method-error">
                                            <label class="custom-control-label" for="atm">ATM Transfer</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input id="credit_card" name="payment_method_id" type="radio" class="custom-control-input" value="2" data-error=".payment_method-error">
                                            <label class="custom-control-label" for="credit_card">Credit Card</label>
                                        </div>
                                    </div>
                                    <div class="payment_method-error"></div>
                            </div>

                            <div class="mb-3">
                                <label for="referral_code">Transfer Order (Rekening Bank Anda)<span class="text-danger">*</span></label>
                                {{-- <select name="bank_from" class="form-control" id="">
                                    <option value="">Pilih Bank</option>
                                    <option value="BNI">BNI</option>
                                    <option value="BCA">BCA</option>
                                    <option value="MANDIRI">MANDIRI</option>
                                </select>
                                <input type="text" class="form-control" id="account_number" name="account_number" placeholder="No Rekening Anda" > --}}
                                <div class="row">
                                    <div class="col">
                                        <select name="bank_from" class="form-control" id="">
                                            <option value="">Pilih Bank</option>
                                            <option value="BNI">BNI</option>
                                            <option value="BCA">BCA</option>
                                            <option value="MANDIRI">MANDIRI</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" id="account_number" name="account_number" maxlength="15" minlength="10" placeholder="No Rekening Anda" >
                                        {{-- <input type="text" class="form-control" placeholder="Last name"> --}}
                                    </div>
                                  </div>

                                <label for="referral_code">Ke Rekening (Rekening Cash Back.com pilihan anda)<span class="text-danger">*</span></label>
                                <select name="bank_to" class="form-control" id="">
                                        <option value="">Pilih Bank</option>
                                        <option value="BNI">BNI</option>
                                        <option value="BCA">BCA</option>
                                        <option value="MANDIRI">MANDIRI</option>
                                </select>
                                <div class="label-code"></div>
                            </div>
                            <div class="mb-3">
                                <label for="referral_code">Upload Bukti Transfer<span class="text-danger">*</span></label> <br>
                                <input type="file" name="image" >
                            </div>


                            <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/additional-methods.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#formmember").validate({
        ignore: [],
        rules: {
            user_id: {
                required: true
            },
            account_name: {
                required: true
            },
            amount: {
                required: true
            },
            payment_method: {
                required: true
            },
            bank_from: {
                required: true
            },
            account_number: {
                required: true,
                digits: true
            },
            bank_to: {
                required: true
            }
            // // customer_code: {
            // //     required: true
            // // },
            // ktp: {
            //     required:true,
            //     digits: true,
            //     minlength: 16,
            //     remote: {
            //         url: "{{url('member/cek_ktp')}}",
            //         type: "get"
            //     }


            // },
            // email: {
            //     required: true,
            //     email: true,
            //     remote: {
            //         url: "{{url('member/cek_email')}}",
            //         type: "get"
            //     }
            // },
            // gender: {
            //     required: true
            // },
            // birth_date: {
            //     required: true
            // },
            // phone: {
            //     required: true,
            //     digits: true
            // },
            // address: {
            //     required: true
            // },
            // password: {
            //     required: true
            // },
            // confirm_password: {
            //     required: true,
            //     equalTo: '#password'
            // },

        },
        messages: {
            // ktp: {
            //     remote: "KTP already exists."
            // },
            // email: {
            //     remote: "Email already exists."
            // }
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#birth_date').datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "1900:+0",
        onSelect: function() {
            $('#birth_date').removeClass('error');
            $('#birth_date + label').remove();
        },
        onClose: function() {
            $(this).trigger("focus").trigger("blur");
        }
    });

    $.ajax({
        type: 'GET',
        url: '{{ route('getPackets') }}',
        dataType: 'json',
        success: function(response) {
            if (response.message = 'success') {
                $.each(response.data, function(i, data) {
                    var el = $('<div class="custom-control custom-radio custom-control-inline"><input id="' + data.type + '" name="amount" type="radio" class="custom-control-input" value="' + data.price + '" data-price="' + data.price + '" data-error=".packets-error"><label class="custom-control-label" for="' + data.type + '">' + data.type + '</label></div>');
                    $('.member-type').append(el);
                });
            }

            $('input[name="amount"]').click(function() {
                if ($(this).is(':checked')) {
                    $('.price').html('<p>Rp. ' + $(this).attr('data-price') + '</p>');
                }
            });

            // getProvinces();
        },
        error: function(err) {
            console.log(err);
        }
    });

});
</script>
@endpush
