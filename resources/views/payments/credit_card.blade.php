@extends('layouts.app')

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
.error {
  color: #dc3545;
}
</style>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Payment by Credit Card') }}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="credit-card" id="form-credit-card" method="post" action="#" aria-label="{{ __('Payment by Credit Card') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="mb-3">
                                    <label for="email">Email<span class="text-danger">*</span></label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                                <div class="mb-3">
                                    <label for="card_number">Card Number<span class="text-danger">*</span></label><br>
                                    <input type="text" class="form-control" id="card_number" name="card_number">
                                </div>
                                <div class="mb-3">
                                    <label for="card_name">Card Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="card_name" name="card_name">
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <label for="month">Expiration Date<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="month" name="month" placeholder="MM">
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="year">&nbsp;</label>
                                        <input type="text" class="form-control" id="year" name="year" placeholder="YY">
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="security_code">Security Code<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="security_code" name="security_code">
                                </div>
                                <div class="mb-3">
                                    <label for="amount">Payment Amount<span class="text-danger">*</span></label>
                                    <p class="form-control">Rp. 1.000.000</p>
                                </div>
                                <hr class="mb-4">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/additional-methods.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#form-credit-card").validate({
        ignore: [],
        rules: {
            email: {
                required: true,
                email: true
            },
            card_number: {
                required: true,
                digits: true
            },
            card_name: {
                required: true
            },
            month: {
                required: true,
                digits: true
            },
            year: {
                required: true,
                digits: true
            },
            security_code: {
                required: true
            },
        },
        errorElement: 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
});
</script>
@endpush
