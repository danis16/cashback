<?php

namespace App\Http\Controllers\Member\Services;

use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users_model;
use App\Models\Member_model;
use App\Models\Address_model;
use App\Models\Referral_Code_History;
use App\Models\Packet;
use App\Http\Controllers\Api\Cashback\CashbackController;


class MailService extends Controller
{
    public function __construct()
    {
        $this->cashback_controller = new CashbackController;
    }

    public static function sendMailVerify($request, $user_groups)
    {
        // return $request->user_groups;

        $email = $request->email;
        $nama  = $request->first_name.' '.$request->last_name;

        // if (empty($request->referral_code)) {
            // $user_id = Users_model::where('email'->$request->email)->first();
            // $referalcode =base64_encode($user_id);
            // $referalcode = 123;
        // } else {
        //     $referalcode = $request->referral_code;
        // }


        $RequestArray = array(
            // 'referral_code' => , tidak ada request, belum di kasi request

            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'ktp' => $request->ktp,
            'birth_date' => $request->birth_date,
            'gender' => $request->gender,
            'phone' => $request->phone,
            'address' => $request->address,
            'customer_code' => $request->customer_code,
            // 'password' => $request->password,
            'email' => $request->email,
            'referral_code' => $request->referral_code,
            'user_groups' => $user_groups
        );

        $konvertToJSON = json_encode($RequestArray);

        $konvertToBase64 = base64_encode($konvertToJSON);


        // Mail::send('email.email', array('email' => $email, 'nama' => $nama), function($m) use ($email) {
        Mail::send('email.email', array('konvertToBase64'=>$konvertToBase64,'konvertToJSON'=>$konvertToJSON,'nama'=>$nama, 'email'=>$email), function($m) use ($email) {
            $m->to($email);
            $m->from('no-reply@cashback.com');
            $m->subject('Email Verifikasi');
        });
    }


    public function get_data_email($data)
    {
       $decodeBase64 = base64_decode($data);
        $member = json_decode($decodeBase64);

        return view('auth.register_after_send_email')->with('data',$member);
    }


    public function update_status_email($email)
    {
        // $user = Users_model::where('email', $email)->first();
        // $user->active = true;
        // $user->update();

        // $member_kosong= Member_model::where('ktp',null)->count();
        // $member_semua= Member_model::count();
        // $count_table = $member_semua-$member_kosong-1;

        // $first_member = Member_model::where('customer_code',1)->first();
        // return redirect('login')->with([
        //     'first_member'=>$first_member,
        //     'count_table'=>$count_table
        //     ]);
        // return $user;
    }

    public function checkout(Request $req, $customer_code, $user_groups)
    {

        if ($customer_code==1) {
            $customer = Member_model::where([
                'customer_code'=>$customer_code,
                'user_groups'=>$req->user_groups-1
                ])->first();
            # code...
        } else if($customer_code==NULL){
            $customer = Member_model::where([
                'customer_code'=>0,
                'user_groups'=>$req->user_groups
                ])->first();
        } else {
            $customer = Member_model::where([
                'customer_code'=>$req->customer_code,
                'user_groups'=>$req->user_groups
                ])->first();
        }

        // return $customer;
        $customer->id_packets = $req->packets;
        $customer->first_name =$req->first_name;
        $customer->last_name = $req->last_name;
        $customer->ktp = $req->ktp;
        $customer->gender = $req->gender;
        $customer->birth_date = $req->birth_date;
        $customer->phone = $req->phone;
        $customer->id_payment_method = $req->payment_method;
        $customer->update();

        $user = Users_model::where('id', $customer->user_id)->first();
        $user->name = $req->first_name;
        $user->email = $req->email;
        $user->password = \Hash::make($req->password);
        $user->update();

        $address = Address_model::where('id', $customer->address_id)->first();
        $address->address = $req->address;
        $address->update();

        $referral_code_history = Referral_Code_History::create([
            'customer_id' => $customer_code,
            'referral_code' => $req->referral_code
        ]);

        // store Referral Count via api ke backend
        $this->cashback_controller->storeReferralCount($req, $customer);

        $packets = Packet::where('id',$req->packets)->first();


        $email = $req->email;
        //referalcode di table ref history
        //email di tabel user , password
        //address di table address

        Mail::send('checkout.checkout_email', array(
            'customer'=>$customer,
            'user'=>$user,
            'address'=>$address,
            'packets'=>$packets
        ), function($m) use ($email) {
            $m->to($email);
            $m->from('no-reply@cashback.com');
            $m->subject('Email Checkout');
        });


        return view('checkout.checkout')
        ->with([
            'customer'=>$customer,
            'user'=>$user,
            'address'=>$address,
            'packets'=>$packets
            ]);
    }

}
