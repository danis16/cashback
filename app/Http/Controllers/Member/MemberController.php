<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Address_model;
use App\Models\Member_model;
use App\Models\Users_model;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function StructureMember(Request $request)
    {
        $auth = Auth::user();
        $memberParent = Member_model::where('user_id', $auth->id)->first();
        $memberChild = Member_model::where([
            'identity' => $memberParent->customer_code,
            'user_groups' => $memberParent->user_groups,
        ])->get();

        $jumlahMemberChild = Member_model::where([
            'identity' => $memberParent->customer_code,
            'user_groups' => $memberParent->user_groups,
        ])->count();

        function halo($dataGroup, $dataUserGroup)
        {
            $dataGroup;
            $memberChild = Member_model::where([
                'groups' => $dataGroup,
                'user_groups' => $dataUserGroup,
            ])->first();

            $jumlahMemberChild = Member_model::where([
                'groups' => $dataGroup,
                'user_groups' => $dataUserGroup,
            ])->count();
            
            //bisa ngga sih $memberChild di tampilin dalam bentuk array ?? atau mungkin kamu bisa bantu kasih aku harus pakai apa wkwkwkwkw
            //Jadi ceritanya, aku mau ambil data 'groups' yang paling kecil di dalam tabel 'customer' yang bernilai kosong berdasarkan kepala yang login.
            //Sistem ini kalo di tampilin bentuknya kayak MLM , nah kepala paling atas itu dia yang login. 
            //Kepala sebagai Parent, nanti anaknya ada 2 sebagai child, identitasnya itu jika 'customer_code' punya kepala = 'identity' di Child
            //nanti child punya anak lagi jadi grandChild dan begitu seterusnya
            
            //rencana nanti di tampilan pohon faktornya itu bisa nge add hanya pada baris terkecil 
            
            

        }

        for ($i = 0; $i < 2; $i++) {
            $memberGrandChild[$i] = Member_model::where([
                'identity' => $memberChild[$i]->customer_code,
                'user_groups' => $memberChild[$i]->user_groups,
                'ktp' => null,
            ])->first();

            $memberGrandChildIsset[$i] = Member_model::where([
                'identity' => $memberChild[$i]->customer_code,
                'user_groups' => $memberChild[$i]->user_groups,
            ])->first();

        }

        if (empty($memberGrandChild[0]->groups)) {
            $dataGroup = $memberGrandChildIsset[0]->groups + 1;
            $dataUserGroup = $memberGrandChildIsset[0]->user_groups;

            halo($dataGroup, $dataUserGroup);

        } else {
            echo $dataGroup = $memberGrandChild[0]->groups;
        }

        die;

// batas tampilan

        $member = Member_model::where('user_id', $auth->id)->first();
        if (isset($member)) {
            $countMember = Member_model::where([
                'identity' => $member->customer_code,
                'user_groups' => $member->user_groups,
            ])->count();
        } else {
            $countMember = 0;
        }

        $first_member = Member_model::where('user_id', $auth->id)
            ->whereHas('users', function ($q) {
                $q->where('active', 1);
            })
            ->first();
        // return $dataGroup;

        return view('member.structure')->with([
            'first_member' => $first_member,
            'count_table' => $countMember,
            'id_user' => $auth->id,
            'data_group' => $dataGroup,
        ]);
    }

    public function StructureMemberOne($id)
    {

        $member = Member_model::where('user_id', $id)->first();
        if (isset($member)) {
            $countMember = Member_model::where([
                'identity' => $member->customer_code,
                'user_groups' => $member->user_groups,
            ])->count();
        } else {
            $countMember = 0;
        }

        $first_member = Member_model::where('user_id', $id)
            ->whereHas('users', function ($q) {
                $q->where('active', 1);
            })
            ->first();

        return view('member.structure')->with([
            'first_member' => $first_member,
            'count_table' => $countMember,
            'id_user' => $id,
        ]);
    }

    public function create($value = '')
    {
        $auth = Auth::user();

        $member = Member_model::where('user_id', $auth->id)->first();
        if (isset($member)) {

            $select_position_min = Member_model::where([
                'identity' => $member->customer_code,
                // 'ktp'=>null
            ])->first();

            $select_position_max = Member_model::where([
                'identity' => $member->customer_code,
                // 'ktp'=>null
            ])->get()->max();

            // $select_position_min = $select_position->first();
            // $select_position_max = $select_position_min+1;
            if (empty($select_position_max->ktp)) {
                $select_tampil_max = $select_position_max->customer_code;
            } else {
                $select_tampil_max = null;
            }

            if (empty($select_position_min->ktp)) {
                $select_tampil_min = $select_position_min->customer_code;
            } else {
                $select_tampil_min = null;
            }

            $user_groups = $member->user_groups;
        } else {
            $select_tampil_min = null;
            $select_tampil_max = null;
            $user_groups = null;
        }

        // return $select_tampil_min;
        // return $select_position_max;

        // $select_position = Member_model::where('ktp', null)->get();

        // $referal_code_db = Member_model::where('identity', 1)->first();
        $referal_code_db = Member_model::where('user_id', $auth->id)->first();
        if (isset($referal_code_db)) {
            $referal_code = $referal_code_db;
        } else {
            $referal_code = '';
        }
        // $query =
        // $referal_code = base64_encode();

        return view('member.create')->with([
            // 'select_position' => $select_position,
            'select_tampil_min' => $select_tampil_min,
            'select_tampil_max' => $select_tampil_max,
            'user_groups' => $user_groups,
            'referal_code' => $referal_code,
        ]);

    }

    public function create_member($customer_code, $user_groups)
    {
        $auth = Auth::user();
        $customer = Member_model::where('user_id', $auth->id)->first();
        return view('member.create_member')->with([
            'customer' => $customer,
            'customer_code' => $customer_code,
            'user_groups' => $user_groups,
            'user' => $auth,
        ]);

    }

    public function storeCustomer(Request $req)
    {
        return view('member.cekout');
    }

    public function storeMember(Request $req)
    {

        // jika jumlah tabel nya 0, maka create table, dan menambah 2 field baru
        // jika ada tabelnya, maka update tabel berdasarkan customer_code yang sudah di inputkan dan generate field

        $auth = Auth::user();
        // return $auth->id;
        $count_table = DB::table('customers')->where('customer_code', $req->customer_code)->count();

        if ($count_table == 0) {
            //perintah tambah data
            $user_isi = Users_model::create([
                'name' => $req->first_name,
                'email' => $req->email,
                'password' => \Hash::make($req->password),
                // 'token'=> str_random(25)
            ]);

            $address_isi = Address_model::create([
                'address' => $req->address,
            ]);

            $cust_isi = Member_model::create([
                'referral_code' => base64_encode($user_isi->id),
                'customer_code' => 1,
                'first_name' => $req->first_name,
                'last_name' => $req->last_name,
                'ktp' => $req->ktp,
                'birth_date' => $req->birth_date,
                'gender' => $req->gender,
                'phone' => $req->phone,
                'address_id' => $address_isi->id,
                'user_id' => $user_isi->id,
                'groups' => 1,
                'identity' => '0',
                'user_groups' => 1,
            ]);

            // $user=$req->email;
            ///sending email
            // event(new UserActivationEmail($user));

            //perintah tambah field kosong
            $count_table = DB::table('customers')->count();
            $perulangan = $count_table + 1;
            for ($i = 1; $i <= $perulangan; $i++) {
                $user = Users_model::create([
                    'name' => null,
                    'email' => null,
                    'password' => null,
                ]);

                $address = Address_model::create([
                    'address' => null,
                ]);

                $child = $count_table + $i;
                $cust = Member_model::create([
                    'referral_code' => base64_encode($user->id),
                    'customer_code' => $count_table + $i,
                    'first_name' => null,
                    'last_name' => null,
                    'ktp' => null,
                    'birth_date' => null,
                    'gender' => null,
                    'phone' => null,
                    'address_id' => $address->id,
                    'user_id' => $user->id,
                    'groups' => 2,
                    'identity' => 1,
                    'user_groups' => 1,
                ]);
            }

            // $referalcode = base64_encode($user->id);

            //verifikasi email

            // $user_isi->notify(new VerifyEmail($user_isi));
            // $user_isi->sendVerificationEmail();
            // return $user_isi;

        } else {

            if ($auth->id == 1) {

                $user_group_max = Member_model::max('user_groups');

                //perintah tambah data
                $user_isi = Users_model::create([
                    'name' => $req->first_name,
                    'email' => $req->email,
                    'password' => \Hash::make($req->password),

                ]);

                $address_isi = Address_model::create([
                    'address' => $req->address,
                ]);

                $cust_isi = Member_model::create([
                    'referral_code' => base64_encode($user_isi->id),
                    'customer_code' => 1,
                    'first_name' => $req->first_name,
                    'last_name' => $req->last_name,
                    'ktp' => $req->ktp,
                    'birth_date' => $req->birth_date,
                    'gender' => $req->gender,
                    'phone' => $req->phone,
                    'address_id' => $address_isi->id,
                    'user_id' => $user_isi->id,
                    'groups' => 1,
                    'identity' => '0',
                    'user_groups' => $user_group_max + 1,
                ]);

                // $user=$req->email;
                ///sending email
                // event(new UserActivationEmail($user));

                //perintah tambah field kosong
                $count_table = DB::table('customers')->where('user_groups', $user_group_max + 1)->count();
                $perulangan = $count_table + 1;
                for ($i = 1; $i <= $perulangan; $i++) {
                    $user = Users_model::create([
                        'name' => null,
                        'email' => null,
                        'password' => null,
                    ]);

                    $address = Address_model::create([
                        'address' => null,
                    ]);

                    $child = $count_table + $i;
                    $cust = Member_model::create([
                        'referral_code' => base64_encode($user->id),
                        'customer_code' => $count_table + $i,
                        'first_name' => null,
                        'last_name' => null,
                        'ktp' => null,
                        'birth_date' => null,
                        'gender' => null,
                        'phone' => null,
                        'address_id' => $address->id,
                        'user_id' => $user->id,
                        'groups' => 2,
                        'identity' => 1,
                        'user_groups' => $user_group_max + 1,
                    ]);
                }

                //  $referalcode = base64_encode($user->id);

            } else {

                //perintah update data berdasarkan customer code
                $customer = Member_model::where([
                    'customer_code' => $req->customer_code,
                    'user_groups' => $req->user_groups,

                ])->first();
                // $customer->first_name = $req->first_name;
                // $customer->last_name = $req->last_name;
                // $customer->ktp = $req->ktp;
                // $customer->birth_date = $req->birth_date;
                // $customer->gender = $req->gender;
                // $customer->phone = $req->phone;
                // $customer->update();

                // $user = Users_model::where('id', $customer->user_id)->first();
                // $user->name = $req->first_name;
                // $user->email = $req->email;
                // $user->password = \Hash::make($req->password);
                // $user->update();

                // $address = Address_model::where('id', $customer->address_id)->first();
                // $address->address = $req->address;
                // $address->update();

                //mengambil data group pada field yang di inputkan
                $groups_data = Member_model::where([
                    'groups' => $customer->groups,
                    'user_groups' => $customer->user_groups,
                ])->first();
                $data_group = $groups_data->groups + 1;

                //cek apakah data group nya ada apa engga jika di jumlahkan 1
                $groups_data = Member_model::where([
                    'groups' => $data_group,
                    'user_groups' => $customer->user_groups,
                ])->count();

                if ($groups_data == 0) {

                    // -------------------------------------------------------
                    //------------------------MENAMBAH-----------------------
                    // -------------------------------------------------------

                    // $count_table = DB::table('customers')->count();
                    // $data_group = Member_model::where('groups', $customer->groups)->count();
                    // $perulangan = $data_group;
                    $cust_group = Member_model::where([
                        'groups' => $customer->groups,
                        'user_groups' => $customer->user_groups,
                    ])->get();
                    $customer_code_max_plus = $cust_group->max('customer_code') + 1;
                    $customer_code_min = $cust_group->min('customer_code');
                    $customer_code_max = $cust_group->max('customer_code');

                    //perulangannya gais, coba anda pikirkan sendiri, saya sudah tidak mengingatnya
                    for ($k = $customer_code_min; $k <= $customer_code_max; $k++) {
                        for ($i = 0; $i < 2; $i++) {

                            // $child = Member_model::where('customer_code', $k)->first();

                            $user = Users_model::create([
                                'name' => null,
                                'email' => null,
                                'password' => null,
                            ]);

                            $address = Address_model::create([
                                'address' => null,
                            ]);

                            $cust = Member_model::create([
                                'referral_code' => base64_encode($user->id),
                                'customer_code' => $customer_code_max_plus++,
                                'first_name' => null,
                                'last_name' => null,
                                'ktp' => null,
                                'birth_date' => null,
                                'gender' => null,
                                'phone' => null,
                                'address_id' => $address->id,
                                'user_id' => $user->id,
                                'groups' => $customer->groups + 1,
                                'identity' => $k,
                                'user_groups' => $customer->user_groups,
                            ]);
                        }

                    }

                }
            }
        }

        if (empty($req->user_groups)) {
            $user_groups = Member_model::max('user_groups') + 1;
        } else {
            $user_groups = $req->user_groups;
        }

        $sendMailVerification = \App\Http\Controllers\Member\Services\MailService::sendMailVerify($req, $user_groups);
        return redirect('member/structure');
        // return \App\Http\Controllers\Member\Services\MailService::sendMailVerify($req);

    }

    public function cek_ktp(Request $request)
    {
        $ktp = $request->ktp;

        $cek = Member_model::where('ktp', $ktp)->count();
        if ($cek == 0) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function cek_email(Request $request)
    {
        $email = $request->email;

        $cek = Users_model::where('email', $email)->count();
        if ($cek == 0) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function ref_code()
    {
        $cust_code = $_GET['cust_code'];
        return base64_encode($cust_code);
        // return Member_model::where('');
    }

}
