<?php

namespace App\Http\Controllers\Api\Cashback;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class CashbackService extends Controller
{
    public function __construct()
    {
        $this->url = "http://149.129.243.147/backendcashback/public/api/v1/cashback/";
    }

    public function getPackets()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_PORT => "8080",
          CURLOPT_URL => $this->url . "packets",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 500,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Postman-Token: 432edbe7-55c5-46da-bf5c-7aadade5a076"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error get packets: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }

    public function getProvinces()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_PORT => "8080",
          CURLOPT_URL => $this->url . "provinces",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Postman-Token: 432edbe7-55c5-46da-bf5c-7aadade5a076"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error get provinces: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }

    public function getCities($province_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_PORT => "8080",
          CURLOPT_URL => $this->url . "cities",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "province_id=" . $province_id,
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 432edbe7-55c5-46da-bf5c-7aadade5a076"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error get cities: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }

    public function getDistricts($city_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_PORT => "8080",
          CURLOPT_URL => $this->url . "districts",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "city_id=" . $city_id,
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 432edbe7-55c5-46da-bf5c-7aadade5a076"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error get districts: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }

    public function getVillages($district_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_PORT => "8080",
          CURLOPT_URL => $this->url . "villages",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "district_id=" . $district_id,
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 432edbe7-55c5-46da-bf5c-7aadade5a076"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error get villages: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }

    public function storeCustomer($data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_PORT => "8080",
          CURLOPT_URL => $this->url . "customers/store",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $data,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 87079471-ee0a-4563-82d5-10f714efcaad",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error store customer: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }

    public function storeMember($data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_PORT => "8080",
          CURLOPT_URL => $this->url . "members/store",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $data,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 87079471-ee0a-4563-82d5-10f714efcaad",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error store member: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }

    public function storePaymentConfirmation($data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_PORT => "8080",
          CURLOPT_URL => $this->url . "payments/confirmation/store",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $data,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 87079471-ee0a-4563-82d5-10f714efcaad",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error store payment confirmation: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }

    public function checkKtp($ktp)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_PORT => "8080",
          CURLOPT_URL => $this->url . "check/ktp",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "ktp=" . $ktp,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 87079471-ee0a-4563-82d5-10f714efcaad",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error check ktp: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }

    public function checkEmail($email)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_PORT => "8080",
          CURLOPT_URL => $this->url . "check/email",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "email=" . $email,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 87079471-ee0a-4563-82d5-10f714efcaad",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error check email: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }

    public function checkReferralCode($referral_code)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_PORT => "8080",
          CURLOPT_URL => $this->url . "check/referral-code",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "referral_code=" . $referral_code,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 87079471-ee0a-4563-82d5-10f714efcaad",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error check referral code: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }

    public function storeReferralCount($data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->url . "referral-count/store",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $data,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 87079471-ee0a-4563-82d5-10f714efcaad",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Log::error(json_encode(['Error store referral count: ' . $err]));
            return false;
        } else {
            return $response;
        }
    }
}
