<?php

namespace App\Http\Controllers\Api\Cashback;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Notification;
use App\Notifications\PaymentATM;
use App\Notifications\PaymentCreditCard;
use App\Notifications\VerifyCustomer;
use App\User;

class CashbackController extends Controller
{
    public function __construct()
    {
        $this->service = new CashbackService;
    }

    public function getPackets()
    {
        return $this->service->getPackets();
    }

    public function getProvinces()
    {
        return $this->service->getProvinces();
    }

    public function getCities(Request $request)
    {
        return $this->service->getCities($request->province_id);
    }

    public function getDistricts(Request $request)
    {
        return $this->service->getDistricts($request->city_id);
    }

    public function getVillages(Request $request)
    {
        return $this->service->getVillages($request->district_id);
    }

    public function storeCustomer(Request $request)
    {
        $data = $request->except(['_token']);

        $data = http_build_query($data) . "\n";

        $store = $this->service->storeCustomer($data);

        $user = User::where('email', $request->email)->first();

        try {
            if ($request->payment_method == "ATM Transfer") {
                Notification::send($user, new PaymentATM($user));
            } else {
                Notification::send($user, new PaymentCreditCard($user));
            }

            // $admins = User::whereHas('role', function ($query) {
            //     $query->where('role_id', 1);
            // })->get();

            // Notification::send($admins, new VerifyCustomer($user));

            // $request->session()->flash('alert', 'We have sent an email with a confirmation link to your email address.');

            return redirect()->route('login');
        } catch (Exception $e) {
            // $request->session()->flash('alert', 'Failed to send email to customer.');

            return redirect()->route('login');
        }
    }

    public function storeMember(Request $request)
    {

        try {
            $data = $request->except(['_token']);
            $data = http_build_query($data) . "\n";
            $store = $this->service->storeMember($data);

            $message = "Member Create";
            $status  = "success";

        } catch (Exception $e) {
            $message = $e->getMessage()." ".$e->getFile()." ".$e->getLine();
            $status = 'warning';
            \Log::error(json_encode(['store member error' => $e->getMessage()." ".$e->getFile()." ".$e->getLine()]));
            // $request->session()->flash('alert', 'Failed to send email to customer.');
        }
        return redirect()->route('member.structure')->with(['message' => $message, 'status' => $status ]);
    }


    public function storePaymentConfirmation(Request $request)
    {
        try
        {
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image = base64_encode(file_get_contents($image));
            }

            $data = $request->except(['_token', 'image']);
            $data['image'] = $image;

            $data = http_build_query($data) . "\n";
            $resp = $this->service->storePaymentConfirmation($data);

            return redirect()->route('login');
        }
        catch(\Exception $e)
        {
            \Log::error(json_encode(['file' => $e->getFile(), 'line' => $e->getFile(), 'message' => $e->getMessage()]));
            return back();
        }
    }

    public function checkKtp(Request $request)
    {
        return $this->service->checkKtp($request->ktp);
    }

    public function checkEmail(Request $request)
    {
        return $this->service->checkEmail($request->email);
    }

    public function checkReferralCode(Request $request)
    {
        return $this->service->checkReferralCode($request->referral_code);
    }

    public function storeReferralCount($request, $customer)
    {
        $data = [            
            'referral_code' => $request->referral_code
        ];

        $data = http_build_query($data);

        return $this->service->storeReferralCount($data);   
    }
}
