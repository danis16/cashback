<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Users_model;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        if (auth()->attempt([
            'email' => $request->email,
            'password' => $request->password,
            'active' => 1
            ])) {
            return redirect()->intended('home');
        }

        return redirect()->back()->with(['error' => 'These credentials do not match our records.']);
    }

    public function cek_email_aktif(Request $request)
    {
        $email = $request->email;

        $cek = Users_model::where([
            'email' => $email,
            'active' => 1,
        ])->count();
            // return $email;
        if ($cek == 0) {
            return 'false';
        } else {
            return 'true';
        }

    }

}
