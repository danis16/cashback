<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use DB;
use App\Models\PaymentConfirmation;

class PaymentConfirmationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }

    public function index()
    {
        // $param = request()->segment(3);
        // $decode = base64_decode($param);
        // $explode = explode('-', $decode);

        // $user_id = $explode[0];
        // $email = $explode[1];

        // $payment_confirmation = PaymentConfirmation::where('user_id', $user_id)->count();

        // if ($payment_confirmation > 0) {
        //     return redirect()->route('index');
        // } else {
            // return view('payments.confirmation', compact('user_id', 'email'));
            return view('payments.confirmation');
        // }
    }

    public function create(Request $req)
    {
        $uploadedFile = $req->file('image');
        $path = $uploadedFile->store('public/img_confirm');

            $paymentConfirmation = PaymentConfirmation::create([
                'user_id' => $req->user_id,
                'payment_method_id' => $req->payment_method,
                'date' => Date('Y-m-d'),
                'account_name'=>$req->account_name,
                'amount' => $req->amount,
                'bank_from' => $req->bank_from,
                'account_number' => $req->account_number,
                'bank_to' => $req->bank_to,
                'image' => $path
            ]);
            return view('payments.message_success');


    }
}
