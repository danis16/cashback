<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentConfirmation extends Model
{
    protected $table = "payment_confirmations";

    protected $fillable = [
        'user_id', 'payment_method_id', 'date', 'account_name', 'account_number', 'bank_from', 'bank_to', 'amount', 'image'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function payment_method()
    {
        return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id');
    }
}
