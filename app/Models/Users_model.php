<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\VerifyEmail;

class Users_model extends Model
{
    protected $table = 'users';
    protected $guarded = [];


    public function verified()
    {
        return $this->token === null;
    }

    //Send rhe user a verification email

    public function sendVerificationEmail()
    {
        $this->notify(new VerifyEmail($this));
    }
}
