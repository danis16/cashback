<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Member_model extends Model
{
    protected $table = 'customers';
    protected $guarded = [];

    public function address()
    {
        return $this->hasOne('App\Models\Address_model', 'id', 'address_id ');
    }

    public function getChild()
    {
        $auth = Auth::user();
        $member = Member_model::where('user_id',$auth->id)->first();

        $child = Member_model::where([
            'identity'=> $this->getAttribute('customer_code'),
            'user_groups' => $member->user_groups
        ])
            ->where('customer_code', '!=', $this->getAttribute('customer_code'))
            // ->where('user_groups', '==', $member->user_groups)
            // ->whereHas('users', function($q) {
            //     $q->where('active', 1);
            // })
            ->get();

        return $child;
    }


    public function users()
    {
        return $this->hasOne('App\Models\Users_model', 'id', 'user_id');
    }
}
